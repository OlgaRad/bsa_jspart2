// Pop The Bubble game. Цель – надо лопать пузыри, которые будут продолжать 
// появляться на экране в хаотичном порядке и в хаотичное время. Каждый раз, 
// когда вы лопаете пузырь - ваш счет будет увеличиваться. Для выполнения этого 
// задания вы должны открыть игровую веб-страницу и подождать 5 секунд. Между тем, 
// на экране появятся несколько пузырьков, и вам нужно будет подсчитать количество 
// пузырьков, появившихся за время ожидания, а затем вы должны их все лопнуть 
// ( нажав на пузырь). Создайте expect(), чтобы проверить счет. Бонус: Запустите 
// бесконечный тест, который лопает пузырьки как можно быстрее и проверяет счетчик
// каждый раз, что он увеличивается. Цель - ни одного пузыря на экране. И бесконечно.


function expect() {
	let count = 0;
	setTimeout(() => 
		(setInterval(function counter(){
			for (var i = 0; ; i++) {
				console.log(count++);
			}
			
		},100),5000));

}
console.log(expect());

// Написать функцию superSort(value), которая получает строку слов, 
// разделенную пробелами. Каждое слово может состоять из латинских букв 
// в нижнем регистре и цифр. Возвращаемый результат должен быть строкой 
// из полученных слов, которые отсортированы в алфавитном порядке, 
// разделенными пробелами, при этом во время сортировки при сравнении 
// слов числа должны игнорироваться, как если бы они не существовали. 
// Например: для строки ввода mic09ha1el 4b5en6 michelle be4atr3ice 
// результатом функции должно быть следующее: 
// be4atr3ice 4b5en6 mic09ha1el michelle

function superSort(value) {
	let str = value.toLowerCase();
	let newStr = str.split(' ');
	console.log(newStr);
	newStr.sort(function(a,b) {
		if(parseInt(a) !==Number && parseInt(b) !==Number) {
			if (a<b)
            return -1;
        	if (a>b)
            return 1;
        	return 0;
			}
	})
	let result = newStr.join(' ');
	return result;
}

console.log(superSort("mic09ha1el 4b5en6 michelle be4atr3ice"));

// Написать функцию, которая будет сравнивать два значения. 
// Значения - это даты, могут быть разного формата. Главное, 
// функция должна проверять, что две даты идентичны. Даты могут 
// отличаться, от разных разделительных знаков – «”.” “,” “/” “-“ », 
// до разного расположения года, месяца и дня – «”yyyy.mm.dd” “dd.mm.yyyy” 
// “mm.dd.yyyy”»

function compareDate(date1, date2) {
	let value1 = date1.split(/[/,.-]/);
	value1.sort(function(a,b) {
		return a-b;
	})
	let val1 = value1.join('');

	let value2= date2.split(/[/,.-]/);
	value2.sort(function(a,b) {
		return a-b;
	})
	let val2 = value2.join('');

	if (val1===val2) {
		return "Dates are equal";
	} else 
	return "Dates are different";
}
// console.log(compareDate('1998.05.20', '20.05.1998'));

// Есть массив с множеством чисел. Все числа одинаковы, кроме одного. 
// Попробуйте найти это число! 
// Например, '1, 1, 1, 2, 1, 1' => 2, ' 0, 0, 0,55, 0, 0 ' => 0,55. 
// Гарантируется, что массив содержит более 3 чисел. 
// Тестовые данные могут содержать очень большие массивы, поэтому 
// подумайте о производительности.

let arr = [1,1,1,0.55,1,1];

function getUniqueNumber(arr) {
	return arr.filter(function(item) {
		return arr.indexOf(item) === arr.lastIndexOf(item);
	})[0] || -1;
}

// console.log(getUniqueNumber(arr));

// Используя if..else или условный (тернарный) оператор завершите функцию 
// getDiscount. Функция принимает 1 параметр: number, number - количество 
// единиц товара, которые покупает клиент. При покупке меньше 5 единиц, 
// клиент не получает скидку и должен будет заплатить 100 процентов 
// стоимости товара. Если покупок 5, но не больше 10 (<10), тогда клиент 
// заплатит только 95 процентов от стоимости товара. Если покупок 10 или 
// больше (>=10) - 90%. Функция должна возвращать стоимость товара.


function getDiscount(number) {
	let costItem = 0;
	if(number<5) {
		return 'Скидки нет. Стоимость товара*=number';
	}else if(number>=5 && number<10) {
		return 'Скидка 5%. Стоимость товара*=0,95*number';
	} else if(number>=10) {
		return 'Скидка 10%. Стоимость товара*=0,90*number';
	}

}

console.log(getDiscount(4));
console.log(getDiscount(8));
console.log(getDiscount(10));

// Используя switch statement напишите функцию getDaysForMonth, которая 
// принимает 1 параметр: month - означает месяц года (месяц всегда больше 0, 
// меньше или равен 12). И возвращает количество дней в месяце (e.g. 1 вернет 31). 
// Високосный год учитывать не стоит.

function getDaysForMonth(month) {
	switch (month) {
		case 1:
		return "1- 31 days"
		break;

		case 2:
		return "2- 28 days"
		break;

		case 3:
		return "3- 31 days"
		break;

		case 4:
		return "4- 30 days"
		break;

		case 5:
		return "5- 31 days"
		break;

		case 6:
		return "6- 30 days"
		break;

		case 7:
		return "7- 31 days"
		break;

		case 8:
		return "8- 31 days"
		break;

		case 9:
		return "9- 30 days"
		break;

		case 10:
		return "10- 31 days"
		break;

		case 11:
		return "11- 30 days"
		break;

		case 12:
		return "12- 31 days"
		break;

		default:
		return "You enter wrong number!"
	}
	
}

// console.log(getDaysForMonth(1));
// console.log(getDaysForMonth(2));
// console.log(getDaysForMonth(3));
// console.log(getDaysForMonth(4));
// console.log(getDaysForMonth(5));
// console.log(getDaysForMonth(6));
// console.log(getDaysForMonth(7));